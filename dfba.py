from dfba_sampling.FBA import FBA_model_Wrapper
from dfba_sampling.metamodel import flux_metamodel_Wrapper
from dfba_sampling.dFBA import Constraint_definition
from ode_experiment import OdeExperimentSpec
import numpy as np
import scipy as sc
import ode_system
import gzip, pickle
import argparse
import pandas as pd


def Run(config_ODE='data/EDO_parameters.yml',config_exp="data/dFBA_styphi_fprau_oxygen_parameter.yml",output='results_dfba/', RHS='FBA_model_Wrapper', dict_mm=None):


    '''
    This function let run the simulation either with the surrogate model or the original ODE simulation. It computes FBA
    and then introduce it into the ode system previously defined.

    If this function is run, the output will be the solution of the system.

    The output goes to 'default' in folder 'results_dfba'
    ''' 

    rng = np.random.default_rng(42)
    if RHS == 'FBA_model_Wrapper':
        name_save_file = 'default'
    else:
        name_save_file = 'default.metamodel'    



    Ode = OdeExperimentSpec(config_ODE=config_ODE,config_exp=config_exp)
    parameters = Ode.param
    exp = Ode.exp

    # defining the RHS only once, there must be a prettier way to do this.
    if RHS == 'FBA_model_Wrapper':
        def new_RHS(constraint, bacteria_id, experiment, dict_mm):
            return FBA_model_Wrapper(constraint, bacteria_id, experiment)

    else:
        def new_RHS(constraint, bacteria_id, experiment, dict_mm):
            optimal_flux, flag_err = flux_metamodel_Wrapper(constraint, bacteria_id, dict_mm)
            return optimal_flux[0,:], flag_err


    #y0 = np.append([],rng.uniform(0,0.1,len(parameters['state_variable'])))
    #y0 = 0.1*np.ones(17)
    y0 = 0.1*np.ones(len(parameters['state_variable']))
    #Experiment hypothesis

    for k in parameters['initial_value']:
        y0[parameters['state_index'][k]] = parameters['initial_value'][k]

    y0[parameters['state_index']['neutrophils']] = 0
    y0[parameters['state_index']['neutrophils_e']] = 0

    parameters['time_no_infection'] = [0,40]
    parameters['time_infection'] = [40,90]

    def calculate_fba(x):
        flux_fba = {i: np.zeros(len(exp.compound_name)) for i in exp.species}
        y = x[parameters['state_index_to_FBA_index']]
        #y = np.append(x[2:12], x[0:2])
        constraint = Constraint_definition(y,exp)
        for i in exp.species :
            if x[parameters['state_index'][i]] > 0 : 
                    optimal_flux, flag_err = new_RHS(constraint,i,exp,dict_mm)                    
                    flux_fba[i][parameters['state_index_to_FBA_index']] += optimal_flux
        return flux_fba,constraint
        
    def ode_function(t,x):
        return ode_system.speed_map(x,calculate_fba(x)[0],parameters)

    def ode_function_constraint(t,x):
        flux_fba,constraint = calculate_fba(x)
        return ode_system.speed_map(x,flux_fba,parameters),flux_fba,constraint


    SCIPY_INTEGRATE=False
    if SCIPY_INTEGRATE:
        y0[parameters['state_index']['Styphi']] = 0.0
        sol_no_infection = sc.integrate.solve_ivp(ode_function, parameters['time_no_infection'], y0, method='Radau') 
        print('Simulation with no infection ended')
        T_no_infection = sol_no_infection.t
        Y_no_infection = sol_no_infection.y
        stable_y0 = Y_no_infection[:,-1]
        stable_y0[parameters['state_index']['Styphi']] = parameters['initial_value']['Styphi']
        sol_infection = sc.integrate.solve_ivp(ode_function, parameters['time_infection'],stable_y0, method='Radau') 
        T_infection = sol_infection.t
        Y_infection = sol_infection.y
        T = np.hstack([T_no_infection, T_infection])
        Y = np.hstack([Y_no_infection, Y_infection])
    else:
        y0[parameters['state_index']['Styphi']] = 0.0
        result_y, result_flux, result_constraint = Ode.TimeIntegrate(ode_function_constraint,parameters['time_no_infection'],y0)
        T_no_infection = result_y[:,0]
        Y_no_infection = result_y[:,1:]
        stable_Y0=result_y[-1,1:]
        stable_Y0[parameters['state_index']['Styphi']] = parameters['initial_value']['Styphi']
        result_y_infection, result_flux_infection, result_constraint_infection = Ode.TimeIntegrate(ode_function_constraint,parameters['time_infection'],stable_Y0)
        T_infection = result_y_infection[:,0]
        Y_infection = result_y_infection[:,1:]
        T = np.hstack([T_no_infection, T_infection])
        Y = np.vstack([Y_no_infection, Y_infection])


    if SCIPY_INTEGRATE:
        ######### A posteriori computation of flux
         #flux_but = gamma_but*(Y[parameters['state_index']['_e'],:]-Y[var_key['butyrate_e']]
        save_fba = {i : [] for i in exp.species}
        save_constraint=[] 
        for y in Y.T:
            flux_fba,constraint = calculate_fba(y)
            for i in exp.species:
                save_fba[i].append(flux_fba[i])
            save_constraint.append(constraint[0,[exp.y_id(met) for met in exp.substrate_metabolites]])
        fba_matrix = {i : np.array(save_fba[i])[:,parameters['fba_index'][i]] for i in exp.species}
        TY=np.vstack([T,Y]).T
    else:
        fba_matrix={i:np.vstack([result_flux[i],result_flux_infection[i]])[:,parameters['fba_index'][i]] for i in exp.species}
        save_constraint = np.vstack([result_constraint,result_constraint_infection])[:,[exp.y_id(met) for met in exp.substrate_metabolites]]
        TY = np.vstack([result_y,result_y_infection])

    TY=pd.DataFrame(TY,columns=['t']+[element for element in parameters['state_index']])
    TConstraints=np.hstack([T.reshape(T.shape[0],1),np.array(save_constraint)])
    TConstraints=pd.DataFrame(TConstraints,columns=['t']+[element for element in exp.substrate_metabolites])
    Tfba_matrix={i:pd.DataFrame(np.hstack([T.reshape((T.shape[0],1)),fba_matrix[i]]),columns=['t']+[element for element in parameters[i+'_compounds']]) for i in exp.species}
    with gzip.open(output+ name_save_file, 'wb') as fp:   ##some problem here
        pickle.dump({'parameters':parameters,'Y':TY,'fba_matrix':Tfba_matrix,'constraints':TConstraints},fp)
    fp.close()
    return TY

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description="Command description.")
    parser.add_argument(
        "-ode",
        "--config_ode",
        help="ode parameters: state variable, parameter of the general system (YAML)",
        default='data/EDO_parameters.yml',
        type=str,
    )
    parser.add_argument(
        "-exp",
        "--config_exp",
        help="dfba parameters: initial conditions and exchange reactions (YAML)",
        default="data/dFBA_styphi_fprau_oxygen_parameter.yml",
        type=str,
    )    
    parser.add_argument(
        "-o",
        "--output_folder",
        help="folder for the ode output",
        default= 'data/samples/sample_n/',  #'results_dfba/'
        type=str,
    )
    
    args = parser.parse_args()
        
    Run(config_ODE=args.config_ode,config_exp=args.config_exp,output=args.output_folder, RHS = 'FBA_model_Wrapper', dict_mm = None)
