import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
import matplotlib.patches as mpatches
import gzip, pickle
from cycler import cycler

class TextInMatplotlib(object):

    def __init__(self,text=''):
        self.text=text
    
    def DisplayText(self,ax,text):
        ax.text(0.5, 0.5,text, horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)
        ax.axis('off')

    def AxNumber(self,Number,ax,x=-0.05,y=1.05,fontsize=""):    
        ax.text(x,y, Number, horizontalalignment='center', verticalalignment='center', transform=ax.transAxes, weight='bold',clip_on=False,fontsize=fontsize)




path = '../data/samples/sample_3/'
name_save_file = path+'default'
name_save_file_2 = path+ 'default.metamodel'
with gzip.open(name_save_file, 'rb') as fp:
    locals().update(pickle.load(fp))
with gzip.open(name_save_file_2, 'rb') as fp:
    results_mm = pickle.load(fp)
Y_mm = results_mm['Y']
T=Y['t']
T_mm = Y_mm['t']
fba_matrix_mm = results_mm['fba_matrix']

parameters['cell_population']=['Styphi','Fprau', "neutrophils"]
parameters['epithelial']=['nitric_oxide_e','O2_e','butyrate_e']
parameters['oxygen_butyrate']=['O2','butyrate']                            
state_index = parameters['state_index']
C_but = parameters['C_but']
L = parameters['L']
colors = {'Styphi': 'red',
        'Fprau': 'maroon',
        'O2' : 'deepskyblue',
        'nitric_oxide': 'lawngreen',
        'glucose': 'blue',
        'galactose': 'yellow',
        'thiosulfate': 'purple',
        'galactarate': 'yellow',
        'glucarate': 'blue',
        'nitrate': 'lawngreen',
        'tetrathionate': 'purple',
        'butyrate': 'darkorange',
        'neutrophils': 'darkgreen',
        'nitric_oxide_e': 'lawngreen',
        'O2_e': 'deepskyblue',
        'neutrophils_e': 'darkgreen',
        'butyrate_e': 'darkorange'}
labels  = ['Sth', 'Fprau', 'O2','NO','glucose','galactose','thiosulfate','galactarate','glucarate',
        'nitrate','tetrathionate','butyrate','neutrophils','NO','O2','neutrophils','butyrate']

linestyle = {i :'-' for i in parameters['state_variable']} 
linestyle_mm = {i :'--' for i in parameters['state_variable']} 
luminal_results_cells = Y[parameters['cell_population']]
luminal_results_oxygen_butyrate = Y[parameters['oxygen_butyrate']] 
luminal_results_oxidized = Y[parameters['reduced_molecules']]
luminal_results_reduced = Y[parameters['oxidized_molecules']]
luminal_results_cells_mm = Y_mm[parameters['cell_population']]
luminal_results_oxygen_butyrate_mm = Y_mm[parameters['oxygen_butyrate']] 
luminal_results_oxidized_mm = Y_mm[parameters['reduced_molecules']]
luminal_results_reduced_mm = Y_mm[parameters['oxidized_molecules']]

epithelial_results =  Y[parameters['epithelial']]
epithelial_results_mm =  Y_mm[parameters['epithelial']]

colors_luminal_results_cells = [colors[i] for i in parameters['cell_population']]
colors_fba_Styphi = [colors[i] for i in parameters['Styphi_compounds']]
colors_luminal_results_oxygen_butyrate = [colors[i] for i in parameters['oxygen_butyrate']]
colors_luminal_results_oxidized_reduced = [colors[i] for i in parameters['reduced_molecules']+parameters['oxidized_molecules']]
colors_fba_Fprau = [colors[i] for i in parameters['Fprau_compounds']]
colors_epithelial_results =  [colors[i] for i in parameters['epithelial']]

flux_O2 = parameters['gamma']['O2']*np.abs(Y['O2'] - Y['O2_e'])
flux_nitric_oxide = parameters['gamma']['nitric_oxide']*np.abs(Y['nitric_oxide'] - Y['nitric_oxide_e'])
flux_but= parameters['gamma']['butyrate']*np.abs(Y['butyrate'] - Y['butyrate_e'])

flux_O2_mm = parameters['gamma']['O2']*np.abs(Y_mm['O2'] - Y_mm['O2_e'])
flux_nitric_oxide_mm = parameters['gamma']['nitric_oxide']*np.abs(Y_mm['nitric_oxide'] - Y_mm['nitric_oxide_e'])
flux_but_mm= parameters['gamma']['butyrate']*np.abs(Y_mm['butyrate'] - Y_mm['butyrate_e'])

SMALL_SIZE = 7
MEDIUM_SIZE = 8
BIGGER_SIZE = 10
font={"small":SMALL_SIZE,"medium":MEDIUM_SIZE,"big":BIGGER_SIZE}
plt.rc('font', family='Arial',size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes',titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick',labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
        
unit_width = 5
unit_height= 2.5
 

label_luminal = [labels[state_index[i]] for i in parameters['cell_population']]

index_fba_styphi = fba_matrix['Styphi'].index[T>=parameters['time_no_infection'][-1]]

fig, axs = plt.subplots(3,2, figsize = (2*unit_width, 4*unit_height)) 
axs[0,0].set_prop_cycle(color = colors_luminal_results_cells) 
axs[0,0].plot(T, luminal_results_cells,)
axs[0,0].plot(T_mm, luminal_results_cells_mm, linestyle = '--')
axs[0,0].set_xlabel('Time [hours]')
axs[0,0].set_ylabel('Concentration [g/L]')
axs[0,0].set_title('Luminal Compartment')
patches = []
for count,value in enumerate(parameters['cell_population']):
   patches.append(mpatches.Patch(color=colors[value], label=labels[state_index[value]]))
axs[0,0].legend(handles = patches[:])
axs[0,0].annotate('Salmonella infection', xy=(parameters['time_infection'][0], 0), xytext=(parameters['time_infection'][0]-50, -0.2),
            arrowprops=dict(facecolor='black', shrink=0.05),
            )

axs[0,1].set_prop_cycle(color = colors_luminal_results_oxygen_butyrate)
axs[0,1].plot(T, luminal_results_oxygen_butyrate)
axs[0,1].plot(T_mm, luminal_results_oxygen_butyrate_mm, linestyle = '--')
axs[0,1].set_xlabel('Time [hours]')
axs[0,1].set_ylabel('Concentration [mmol/L]')
axs[0,1].set_title('Luminal Compartment')
patches = []
for count,value in enumerate(parameters['oxygen_butyrate']):
   patches.append(mpatches.Patch(color=colors[value], label=labels[state_index[value]]))
axs[0,1].legend(handles = patches[:])

axs[1,0].set_prop_cycle(color = colors_luminal_results_oxidized_reduced)
axs[1,0].plot(T, luminal_results_reduced)
axs[1,0].plot(T_mm, luminal_results_reduced_mm, linestyle = '--')
axs[1,0].set_xlabel('Time [hours]')
axs[1,0].set_ylabel('Concentration [mmol/L]')
axs[1,0].set_title('Luminal Compartment: reduced molecules')
patches = []
for count,value in enumerate(parameters['reduced_molecules']):
   patches.append(mpatches.Patch(color=colors[value], label=labels[state_index[value]]))
axs[1,0].legend(handles = patches[:])

axs[1,1].set_prop_cycle(color = colors_luminal_results_oxidized_reduced)
axs[1,1].plot(T, luminal_results_oxidized)
axs[1,1].plot(T_mm, luminal_results_oxidized_mm,linestyle = '--')
axs[1,1].set_xlabel('Time [hours]')
axs[1,1].set_ylabel('Concentration [mmol/L]')
axs[1,1].set_title('Luminal Compartment: oxidized molecules')
patches = []
for count,value in enumerate(parameters['oxidized_molecules']):
   patches.append(mpatches.Patch(color=colors[value], label=labels[state_index[value]]))
axs[1,1].legend(handles = patches[:])

axs[2,0].set_prop_cycle(color = colors_epithelial_results)
axs[2,0].plot(T, epithelial_results)
axs[2,0].plot(T_mm, epithelial_results_mm, linestyle = '--')
ax2 = axs[2,0].twinx()
ax2.plot(T,Y['neutrophils_e'],color = colors['neutrophils_e'])
ax2.set_ylabel('Biomass [g/L]')
axs[2,0].set_xlabel('Time [hours]')
axs[2,0].set_ylabel('Concentration [mmol/L]')
axs[2,0].set_title('Epithelial Compartment') 
patches = []
for value in parameters['epithelial']+['neutrophils_e']:
   patches.append(mpatches.Patch(color=colors[value], label=labels[state_index[value]]))
axs[2,0].legend(handles = patches[:])

axs[2,1].set_prop_cycle(color = [colors['O2'], colors['butyrate'], colors['nitric_oxide']])
axs[2,1].plot(T,np.array([flux_O2, flux_but, flux_nitric_oxide]).T)
axs[2,1].plot(T,np.array([flux_O2_mm, flux_but_mm, flux_nitric_oxide_mm]).T,ls = '--')

axs[2,1].set_xlabel('Time [hours]')
axs[2,1].set_ylabel('Flow [mmol/Lh]')
axs[2,1].set_title('Flows between luminal and epithelial compartment')
patches = []
for value in ['O2', 'butyrate', 'nitric_oxide']: 
   patches.append(mpatches.Patch(color=colors[value], label=labels[state_index[value]]))
axs[2,1].legend(handles = patches[:], loc = 'center right')

dfba_legend = mlines.Line2D([], [], color='black', linestyle='-',
                          markersize=15, label='dfba simulation')
metamodel_legend = mlines.Line2D([], [], color='black', linestyle='--',
                          markersize=15, label='Metamodel simulation')
fig.legend(handles = [dfba_legend, metamodel_legend], loc = 'lower center', prop={'size': 9})


numbering='abcdefghijklmnopqrstuvwxyz'
TM=TextInMatplotlib()
for i in range(axs.shape[0]):
    for j in range(axs.shape[1]):
                    TM.AxNumber(numbering[i*axs.shape[1]+j],axs[i,j],y=1.05,x=-0.1,fontsize=BIGGER_SIZE)    


plt.tight_layout()
fig.savefig('comparison_results.pdf')

fig, axs = plt.subplots(2,2)
patches = []
for j in ['Styphi', 'O2']:
    axs[0,0].plot(fba_matrix['Styphi'].loc[index_fba_styphi]['t'],fba_matrix['Styphi'].loc[index_fba_styphi][j], color = colors[j])
    axs[0,0].plot(fba_matrix_mm['Styphi'].loc[index_fba_styphi]['t'],fba_matrix_mm['Styphi'].loc[index_fba_styphi][j], color = colors[j],ls = '--')
    patches.append(mpatches.Patch(color = colors[j], label = labels[state_index[j]]))
axs[0,0].legend(handles = patches[:])
axs[0,0].set_ylabel('Yields [moles/g of Biomass]')
axs[0,0].set_title('Salmonella yields in time')
patches = []
for j in parameters['reduced_molecules']:
    axs[0,1].plot(fba_matrix['Styphi'].loc[index_fba_styphi]['t'],fba_matrix['Styphi'].loc[index_fba_styphi][j], color = colors[j])
    axs[0,1].plot(fba_matrix_mm['Styphi'].loc[index_fba_styphi]['t'],fba_matrix_mm['Styphi'].loc[index_fba_styphi][j], color = colors[j],ls = '--')
    patches.append(mpatches.Patch(color = colors[j], label = labels[state_index[j]]))
axs[0,1].legend(handles = patches[:])
axs[0,1].set_title('Salmonella yields in time: reduced molecules')
patches = []
for j in parameters['oxidized_molecules']:
    axs[1,0].plot(fba_matrix['Styphi'].loc[index_fba_styphi]['t'],fba_matrix['Styphi'].loc[index_fba_styphi][j], color = colors[j])
    axs[1,0].plot(fba_matrix_mm['Styphi'].loc[index_fba_styphi]['t'],fba_matrix_mm['Styphi'].loc[index_fba_styphi][j], color = colors[j],ls = '--')
    patches.append(mpatches.Patch(color = colors[j], label = labels[state_index[j]]))
axs[1,0].legend(handles = patches[:])
patches = []
axs[1,0].set_ylabel('Yields [moles/g of Biomass]')
axs[1,0].set_xlabel('Time [hours]')
axs[1,0].set_title('Salmonella yields in time: oxidized molecules')
for j in parameters['Fprau_compounds']:
    patches.append(mpatches.Patch(color = colors[j], label = labels[state_index[j]]))
axs[1,1].set_prop_cycle(color = colors_fba_Fprau)
axs[1,1].plot(fba_matrix['Fprau']['t'],fba_matrix['Fprau'].drop('t',axis=1))
axs[1,1].plot(fba_matrix_mm['Fprau']['t'],fba_matrix_mm['Fprau'].drop('t',axis=1), ls = '--')
axs[1,1].legend(handles = patches[:])
axs[1,1].set_xlabel('Time [hours]')
axs[1,1].set_title('Fprau yields in time')
fig.legend(handles = [dfba_legend, metamodel_legend], loc = 'center', prop={'size': 8})
fig.tight_layout()
plt.show()
