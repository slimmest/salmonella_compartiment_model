import pandas as pd
from dfba_sampling.metamodel import Metamodel
from pathlib import Path
from ode_experiment import OdeExperimentSpec
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import r2_score
import yaml

def QQplot(mu_dict=None,path_to_constraints=None, path_to_fluxes=None, output = '../data/samples/hyperparameter_selection/', iter_max=150, config_ODE='../data/EDO_parameters.yml',config_exp="../data/dFBA_styphi_fprau_oxygen_parameter.yml", max_order=2,n_obs=400):

        SMALL_SIZE = 7
        MEDIUM_SIZE = 8
        BIGGER_SIZE = 10
        font={"small":SMALL_SIZE,"medium":MEDIUM_SIZE,"big":BIGGER_SIZE}
        plt.rc('font', family='Arial',size=BIGGER_SIZE)          # controls default text sizes
        plt.rc('axes',titlesize=MEDIUM_SIZE)     # fontsize of the axes title
        plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
        plt.rc('xtick',labelsize=SMALL_SIZE)    # fontsize of the tick labels
        plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
        plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
        plt.rc('figure', titlesize=MEDIUM_SIZE)  # fontsize of the figure title
        
        unit_width = 5
        unit_height= 2.5


        Ode = OdeExperimentSpec(config_ODE=config_ODE,config_exp=config_exp)
        parameters = Ode.param
        exp = Ode.exp
        bact=[path.split('_')[-1].split('.')[0].capitalize() for path in path_to_fluxes]
        mm = [Metamodel(kernel_type="Matern", bact=bact[i],exp=exp) 
                          for i in range(len(path_to_fluxes))
                          ]

        c_t = pd.read_csv(path_to_constraints, sep='\t', index_col=0).iloc[n_obs:]
        f_t = [pd.read_csv(path_to_fluxes[i], sep='\t', index_col=0).iloc[n_obs:].values for i in range(len(path_to_fluxes))]
        ypred = np.zeros([len(bact), c_t.shape[0], len(exp.compound_name)])
        for i_s, s in enumerate(bact):
            mm[i_s].new_constraintlearningset(path_to_constraints, n_obs=n_obs, index_col=0)
            mm[i_s].new_fluxlearningset(path_to_fluxes[i_s], n_obs=n_obs, index_col=0)    
            mm[i_s].P_MaxOrder(max_order)
            test=f_t[i_s]
            substrate = mm[i_s].substrate_metabolites
            compound_output = mm[i_s].compound_name
            ncomp = len(compound_output)
            if type(mu_dict) is dict:
                theta_dict = {k: output+f"{iter_max}/mu_{mu_dict[s][k]}/species_{s.lower()}_results.npz" for k in mu_dict[s].keys()}
            else:
                theta_dict = mu_dict+f"species_{s.lower()}_results.npz"
            mm[i_s].load_attributes(theta=theta_dict)
            for i,c in enumerate(c_t.values):
                ypred[i_s,i,:] = mm[i_s].flux_estimate(c[None,:])

            n_b=ncomp//2

            fig, axs = plt.subplots(n_b,2,figsize=(2*unit_width,n_b*unit_height))
            for i_compound in range(ncomp):
                i_axs = i_compound%n_b
                j_axs = i_compound//n_b

                axs[i_axs,j_axs].annotate(f"r2: {np.round(r2_score(test[:,exp.y_id(mm[i_s].compound_name[i_compound])], ypred[i_s, :, exp.y_id(mm[i_s].compound_name[i_compound]) ]),3)}",
                    xy=(1, 0),
                    color="green",
                    xycoords='axes fraction',
                    horizontalalignment='right', verticalalignment='bottom', )
                axs[i_axs,j_axs].scatter(ypred[i_s, :, exp.y_id(mm[i_s].compound_name[i_compound]) ], test[:, exp.y_id(mm[i_s].compound_name[i_compound]) ], alpha=.3)
                axs[i_axs,j_axs].plot([ypred[i_s, :, exp.y_id(mm[i_s].compound_name[i_compound]) ].min(), ypred[i_s, :, exp.y_id(mm[i_s].compound_name[i_compound]) ].max()],
                [ypred[i_s,:, exp.y_id(mm[i_s].compound_name[i_compound]) ].min(), ypred[i_s, :, exp.y_id(mm[i_s].compound_name[i_compound]) ].max()],
                color='red')
                axs[i_axs,j_axs].set_title(mm[i_s].compound_name[i_compound])
                axs[i_axs,j_axs].set_xlabel(r"$\hat{\mathcal{F}}(c)$")
                axs[i_axs,j_axs].set_ylabel(r"$\mathcal{F}(c)$")
            fig.tight_layout()
            fig.savefig(output+s+'_qqplot.pdf')

            if s == 'Fprau':
                fig, axs = plt.subplots(1,2,figsize=(2*unit_width,unit_height))
                axs= axs.reshape((1,2))
            else:
                fig, axs = plt.subplots(n_b,2,figsize=(2*unit_width,n_b*unit_height))
            for i_compound in range(ncomp):
            
                if s == 'Fprau':
                    j_axs = i_compound%n_b
                    i_axs = i_compound//n_b
                else:
                    i_axs = i_compound%n_b
                    j_axs = i_compound//n_b
                if mm[i_s].compound_name[i_compound] in c_t.columns:
                    axs[i_axs,j_axs].scatter(c_t[mm[i_s].compound_name[i_compound]], test[:, exp.y_id(mm[i_s].compound_name[i_compound]) ], alpha=.3, c='C0')
                    axs[i_axs,j_axs].scatter(c_t[mm[i_s].compound_name[i_compound]], ypred[i_s, :, exp.y_id(mm[i_s].compound_name[i_compound]) ], alpha=0.3, c = 'C1')
                    axs[i_axs,j_axs].set_title(mm[i_s].compound_name[i_compound])
                    axs[i_axs,j_axs].set_xlim(-5,0)
            axs[-1,-1].scatter(0,-6,alpha=0.3,c='C0',label = r"$\mathcal{F}(c)$")
            axs[-1,-1].scatter(0,-6,alpha=0.3,c='C1',label = r"$\hat{\mathcal{F}}(c)$")
            axs[-1,-1].set_ylim(-5,0)
            if s == 'Styphi':
                axs[-1,-1].axis('off')
            axs[-1,-1].legend(loc='upper left')
            fig.tight_layout()
            fig.savefig(output+s+'_response.pdf')                
if __name__ == "__main__":

    path_to_constraints = "data/samples/database_subsample_2000_bounds_constraints.csv"
    path_to_fluxes_styphi = "data/samples/database_subsample_2000_bounds_fluxes_Styphi.csv"
    path_to_fluxes_fprau = "data/samples/database_subsample_2000_bounds_fluxes_Fprau.csv"

    path_to_fluxes = [path_to_fluxes_styphi, path_to_fluxes_fprau]
    
#    with open('data/samples/observation_selection/mu_dictionary.pickle', 'r') as f:
#        mu_dict = yaml.safe_load(f)
    mu_dict = 'data/selected_metamodel_'
    QQplot(mu_dict=mu_dict, path_to_constraints=path_to_constraints, path_to_fluxes=path_to_fluxes, n_obs=500)
