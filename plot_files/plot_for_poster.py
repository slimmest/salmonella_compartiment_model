import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
import matplotlib.patches as mpatches
import gzip, pickle
from cycler import cycler
path = '../data/samples/sample_5/'
name_save_file = path+'default'
name_save_file_2 = path+ 'default.metamodel'
with gzip.open(name_save_file, 'rb') as fp:
    locals().update(pickle.load(fp))
with gzip.open(name_save_file_2, 'rb') as fp:
    results_mm = pickle.load(fp)
Y_mm = results_mm['Y']
T=Y['t']
T_mm = Y_mm['t']
fba_matrix_mm = results_mm['fba_matrix']

parameters['cell_population']=['Styphi','Fprau', "neutrophils"]
parameters['epithelial']=['nitric_oxide_e','O2_e','butyrate_e']
parameters['oxygen_butyrate']=['O2','butyrate']                            
state_index = parameters['state_index']
C_but = parameters['C_but']
L = parameters['L']
colors = {'Styphi': 'red',
        'Fprau': 'maroon',
        'O2' : 'deepskyblue',
        'nitric_oxide': 'lawngreen',
        'glucose': 'blue',
        'galactose': 'yellow',
        'thiosulfate': 'purple',
        'galactarate': 'yellow',
        'glucarate': 'blue',
        'nitrate': 'lawngreen',
        'tetrathionate': 'purple',
        'butyrate': 'darkorange',
        'neutrophils': 'darkgreen',
        'nitric_oxide_e': 'lawngreen',
        'O2_e': 'deepskyblue',
        'neutrophils_e': 'darkgreen',
        'butyrate_e': 'darkorange'}
labels  = ['Salmonella', 'Faecalibacterium', 'O2','NO','glucose','galactose','thiosulfate','galactarate','glucarate',
        'nitrate','tetrathionate','butyrate','neutrophils','NO','O2','neutrophils','butyrate']

linestyle = {i :'-' for i in parameters['state_variable']} 
linestyle_mm = {i :'--' for i in parameters['state_variable']} 
luminal_results_cells = Y[parameters['cell_population']]
luminal_results_oxygen_butyrate = Y[parameters['oxygen_butyrate']] 
luminal_results_oxidized = Y[parameters['reduced_molecules']]
luminal_results_reduced = Y[parameters['oxidized_molecules']]
luminal_results_cells_mm = Y_mm[parameters['cell_population']]
luminal_results_oxygen_butyrate_mm = Y_mm[parameters['oxygen_butyrate']] 
luminal_results_oxidized_mm = Y_mm[parameters['reduced_molecules']]
luminal_results_reduced_mm = Y_mm[parameters['oxidized_molecules']]

epithelial_results =  Y[parameters['epithelial']]
epithelial_results_mm =  Y_mm[parameters['epithelial']]

colors_luminal_results_cells = [colors[i] for i in parameters['cell_population']]
colors_fba_Styphi = [colors[i] for i in parameters['Styphi_compounds']]
colors_luminal_results_oxygen_butyrate = [colors[i] for i in parameters['oxygen_butyrate']]
colors_luminal_results_oxidized_reduced = [colors[i] for i in parameters['reduced_molecules']+parameters['oxidized_molecules']]
colors_fba_Fprau = [colors[i] for i in parameters['Fprau_compounds']]
colors_epithelial_results =  [colors[i] for i in parameters['epithelial']]

flux_O2 = parameters['gamma']['O2']*np.abs(Y['O2'] - Y['O2_e'])
flux_nitric_oxide = parameters['gamma']['nitric_oxide']*np.abs(Y['nitric_oxide'] - Y['nitric_oxide_e'])
flux_but= parameters['gamma']['butyrate']*np.abs(Y['butyrate'] - Y['butyrate_e'])

flux_O2_mm = parameters['gamma']['O2']*np.abs(Y_mm['O2'] - Y_mm['O2_e'])
flux_nitric_oxide_mm = parameters['gamma']['nitric_oxide']*np.abs(Y_mm['nitric_oxide'] - Y_mm['nitric_oxide_e'])
flux_but_mm= parameters['gamma']['butyrate']*np.abs(Y_mm['butyrate'] - Y_mm['butyrate_e'])

SMALL_SIZE = 14
MEDIUM_SIZE =18 
BIGGER_SIZE =22 
font={"small":SMALL_SIZE,"medium":MEDIUM_SIZE,"big":BIGGER_SIZE}
plt.rc('font', family='Arial',size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes',titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick',labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
        
unit_width = 5
unit_height= 2.5
 

label_luminal = [labels[state_index[i]] for i in parameters['cell_population']]

index_fba_styphi = fba_matrix['Styphi'].index[T>=parameters['time_no_infection'][-1]]
fig, axs = plt.subplots(1,1, figsize = (2*unit_width, 4*unit_height)) 
axs.set_prop_cycle(color = colors_luminal_results_cells) 
axs.plot(T, luminal_results_cells,)
axs.plot(T_mm, luminal_results_cells_mm, linestyle = '--')
axs.set_xlabel('Time [hours]')
axs.set_ylabel('Concentration [g/L]')
axs.set_title('Luminal Compartment Community Dynamics')
patches = []
for count,value in enumerate(parameters['cell_population']):
   patches.append(mpatches.Patch(color=colors[value], label=labels[state_index[value]]))
axs.legend(handles = patches[:])
axs.annotate('Salmonella infection', xy=(parameters['time_infection'][0], 0), xytext=(parameters['time_infection'][0]-50, -0.1),
            arrowprops=dict(facecolor='black', shrink=0.05),
            )


dfba_legend = mlines.Line2D([], [], color='black', linestyle='-',
                          markersize=15, label='dfba simulation')
metamodel_legend = mlines.Line2D([], [], color='black', linestyle='--',
                          markersize=15, label='Metamodel simulation')
fig.legend(handles = [dfba_legend, metamodel_legend], loc = 'center right', prop={'size': BIGGER_SIZE})
plt.tight_layout()
fig.savefig('ecological_dynamics.pdf')
plt.show()
