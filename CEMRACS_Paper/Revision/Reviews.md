## REFEREE 1:


This paper does regularised reproducing kernel Hilbert space (RKHS) regression to construct an approximation to a flux balance analysis model used in a microbial ecology application. I have no expertise to evaluate the application and will focus on the mathematical and methodological aspects of teh paper.


There are a few points regarding methodology that I think it would be helpful to address in more detail or more explicitly (some of these comments may stem from my incomplete understanding of other parts of the paper):


1. If the weight matrix W = K_p^{1/2} is used in Section 3.4, the solution \alpha^* to the RKHS regression problem in Eq. (17) is available in closed form via simple linear algebra and no ANOVA decomposition needs to be done. Why is this option not considered? Elsewhere in the article (e.g., Section 7) the reasons for using the ANOVA decomposition are discussed, but the it would be very helpful to move or include some of this discussion in Section 3 because now that section gives the impression that the ANOVA decomposition is used only to make it easier to solve the optimisation problem in Eq. (15).


2. To construct the ANOVA kernels one needs to compute integrals of the original kernels in the equaation before Eq. (18) on p11. How are these integrals computed?


3. There is little information about the kernels used to construct the RKHS model. The only thing I could find is a remark on p12 that "we will use Matern kernels". Are the smoothness and lenthscale parameters of the Matern kernels fixed or are they estimated from the data?


Miscellaneous comments:


- When sections are being referred to, only the section number is given instead of "Section [section number]".

- p6: "The terms ... correspond to consumption or production ..." There are no such terms in Equation (9).

- p7: "The term ... in equation (10) ..." There is no such term in Equation (10).

- p7: "gaussian"

- Sec 3.1: I am not sure what "the set of parts of [a set]" means.

- Sec 3.1: A more specific reference to the book [33] would be helpful (e.g., which chapter).

- Sec 3.1: It is not even cursorily described how the ANOVA terms m_{j,p} and x_p are defined.

- Sec 3.2: "definite positive" should probably be "positive definite" (at least I have never encountered this term in the form used in this article).

- Sec 3.2: It is not clear what "the space of definite-positive matrix" is. I suppose this refers to positive-definite kernels.

- Theorem 1: This does not quite define the RKHS: the RKHS is not the space H_k given in the theorem but its completion w.r.t. in the H_k-norm defined in the theorem.

- p10: The use of logical quantifiers seems unnecessary here and writing them out would make the text more understandable and pleasant to read.

- Sec 3.3: A word must be missing on the first line ("N^up X").

- p11: The reference to [4] ought to be more specific.

- Sec 3.4: It is stated that for W = K_P^{1/2} the terms in the regularisation term are RKHS norms of f_p. What are f_p here? Until this point f and its ANOVA decomposition terms f_p have been some generic elements of the RKHSs.

- p11: "Ones"






## REFEREE 2:


In this article the authors present a method to approximate the solution of Flux Balance Analysis model in order to accelerate a metabolic model of Salmonella infection of the gut.

Major comments:

1) The structure of the article makes it difficult to read. Since the metamodel is an approximation of the FBA model, I suggest to first introduce FBA models, then present the proposed approximation and finally present the application to Salmonella infection. This will clarify the paper and help the reader to draw links between the metamodel and the FBA model.

2) Another key aspect that is missing in the paper is a discussion of the computation time. Indeed, if it is initially small (0.01 seconds for example) a speed-up of 45 is not as impressive as if initially the computation time is large (1h for example).

3) Regarding the ode model:

- The population dynamic model description is very short, and the links between the FBA and the differential equation need to be explained more precisely. 

- The choices of state variables need to be explained with respect to the biological context, otherwise the reader can’t understand why all these variables are required. 

- If the ode model presented in this work is new, it really needs to be detailed and explained. If the model was already published elsewhere, the main assumptions need to be recalled and the reader should be addressed to the previous work presenting the model(s). 

- Overall, It is difficult to understand what is a new contribution and what is not.


Other Comments.

* page2 : in the equation following eq (1) there is a discussion about $\nu_b$ but this variable is not in the equation. Please clarify.

* page 3 : equation (2), is $\nu = \nu_b$ in the equation?

* page 6 : equations (8) and (9): is the parameter D the same in both equations? And in equation (9), the same parameters are taken for all the metabolites concentrations $m_l$ ?

* page 6: the authors write “The term $d_n m_n$ is the death rate of neutrophils.” but no such term is in equation (8), and it seems there are 2 death terms, or one death term and one transport term. Please correctly explain the model equations.

* page 7 : the authors write “We can see that the dynamical system renders all the qualitative behaviour of Sth infection as described in the literature (see Fig. 1b).” : it is difficult to compare time dynamical model outputs with a schematic representation.

* page 12 : section 4, on learning database definition, is very hard to understand. In addition, why is it important to “enrich the database in the distribution limits” for metabolic modelling?

* pages 13-14 : figures 3 and 4 are very difficult to read without more explanations – the authors should improve the description to help the reader know which curves should be compared with what, etc.

* page 18 discussion section : Will the hyperparameters selection depend on the learning database?

* page 18 - discussion section : Would you expect, in a spatial model, the impact of replacing the FBA by its metamodel to be as small as the observation you made on the temporal ode model presented here?

* All figure legends must give a precise description of what is plotted.
