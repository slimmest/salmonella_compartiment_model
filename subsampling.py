import pandas as pd
import numpy as np
from scipy.stats import gaussian_kde
import matplotlib.pyplot as plt
import pickle, gzip
from dfba_sampling.experiment import ExperimentSpec
from dfba_sampling.FBA import FBA_model_Wrapper
from dfba_sampling.dFBA import Constraint_definition



#C = pd.read_csv('data/samples/database_constraints.tsv',sep='\t')
#F = pd.read_csv('data/samples/database_fluxes_Fprau.tsv',sep='\t')
#S = pd.read_csv('data/samples/database_fluxes_Styphi.tsv',sep='\t')


#Here we can add all the organsims we want to sample



class Subsampler():

    def __init__(self, path2samples = 'data/samples/', sample_folder_name = 'sample_',sample_file_name = 'default', X_name = 'constraints', number_of_subsample_points = 1999, number_of_samples = 5, experiment = ExperimentSpec(config="data/dFBA_styphi_fprau_oxygen_parameter.yml",networks=["data/Salmonella_FBA.xml", "data/Fprau_FBA.xml"])):
        
        '''
        This class recieves paths, names and an experiment. With them it has all the information to take subsamples from the samples taken in the batch experiments.
        '''
        
        self.exp = experiment

        self.X_name = X_name
        self.Y_names = self.exp.species
        self.path_sample = path2samples
        self.sample_folder_name = sample_folder_name
        self.sample_file_name = sample_file_name
        self.number_points = number_of_subsample_points
        self.number_of_samples = number_of_samples

        self.path_X = self.path_sample + f'database_subsample_{self.number_points}_bounds_' + self.X_name 
        self.paths_Y = []        
        for organism_name in self.Y_names:
            self.paths_Y.append(self.path_sample + f'database_subsample_{self.number_points}_bounds_fluxes_' + organism_name)

        
        

    def ComputeProb(self, x,kde):

        '''  
        This method is not being used but ideally its purpose was to find probability distributions of the samples in order to take a subsample that represent in the best way possible the statistical properties of the sample.
        '''

        kx = x.sort_values()
        f_x = kde(kx)
        sample_probs = f_x/np.sum(f_x)
        return sample_probs, kx    
    
    def create_paths(self, index): 

        '''
        With this function the paths given in the initialization are concatenated either for the constraints as for the organsims.
        '''

        complete_path = self.path_sample + self.sample_folder_name + str(index) + '/'
        path_sample = complete_path + self.sample_file_name
        
        return path_sample
    def read_sample(self, index):

        '''
        Knowing that samples are saved as compressed files, this function decompresses the files, which contains in 'Y' key all the state variables throwed out by the ode solver and in 'constraints' key it has the domain of the mapping F, eg., the constraints for each metabolite at each timestamp.
        Also, this routine is in charge of generate dataframes for each organismm so the table 'Y' is divided according to relation of each bacteria with their metabolites (using the indications stored in the experiment attribute). 
        '''

        path_sample = self.create_paths(index)  #we are only calling the function above

        gzip_bin_file = gzip.open(path_sample,'rb')  #lets read our binary compressed file which comes from dfba function
        sample = pickle.load(gzip_bin_file)  #sample is a dictionary which contains in the column 'Y' the solution values for each domain data point and in 'constraints' the domain values
        gzip_bin_file.close()

        sample_X = sample[self.X_name]  #this should be changed to a generalized name
        sample_Y = sample['fba_matrix']
        print(sample_Y)
        sample_list_Y = []
        
        #mets = dict()                   #in this, the metabolites will be stored
        #total_spec = self.exp.species
        #for sub in self.exp.substrate_metabolites:
        #    mets[sub] = [1 if spec in self.exp.substrate_species(sub) else 0 for spec in total_spec]
#
        #for out in self.exp.output_metabolites:
        #    mets[out] = [0 for spec in total_spec]
        #    
        #for i_spec, spec in enumerate(total_spec):    
        #    try:        
        #        out_dict = self.exp.output_reaction(spec)        
        #        for output in out_dict.keys():
        #            mets[output][i_spec] = 1
        #    except:
        #        pass
        #    
#
#
        #met_selector = pd.DataFrame.from_dict(mets)    #after the routine above, a pandas array is created and contains a binary table for each bacteria, indicating if one bacteria is related with one metabolite.
        #for spec in total_spec:
        #    met_selector[spec] = [1 if s == spec else 0 for s in total_spec]
        #df = met_selector
        #for spec in total_spec:
        #    unordered_desired_df = sample_Y * (df[df[spec] == 1].iloc[0])    #For each bacteria we end up only with 'their'metabolites.
        #    ordered_desired_df = unordered_desired_df[df.keys()]
        #    sample_list_Y.append(ordered_desired_df)

        for spec in sample_Y.keys():
            sample_list_Y.append(sample_Y[spec])
        

        return sample_X, sample_list_Y
    
    def sample(self):
        
        '''
        As the name indicates, this function does the subsampling, but first, it has to assemble all the samples taken by the batch experiment for the constraints and for each organism. 
        An important idea of the sampling is that as the data is concentrated in the margins of the distribution, samples must be taken at the borders than at the centers to conserve information (that's because in ODE simulation there's more data of the stability phase than the rest of the simulation).
        '''

        samples_X = pd.DataFrame()
        samples_list_Y = [pd.DataFrame() for spec in self.exp.species]  #It is an initialized list made of empty dataframes, which will be added when reading the samples.
        concat_mapping = lambda x: pd.concat([x[0], x[1]], axis = 0, ignore_index= True)

        ### ASSEMBLING (avengers xd) ###
        for index in range(self.number_of_samples):
            x,y = self.read_sample(index)
            try:
                sample_X, sample_list_Y = self.read_sample(index)  #reading

                samples_X = pd.concat([samples_X, sample_X], axis = 0, ignore_index= True)   #concatenating
                samples_list_Y = list(map(concat_mapping, list(zip(samples_list_Y, sample_list_Y))))
            except:
                print(f'Sample {index} has a problem!')     #Sometimes the batching experiment can have a problem or there are some remanent samples (from old experiment) in the folders and to not crush the subsample in the middle we will just throw a warning for that specific sample.
        print("end of stacking")
        print(len(samples_X))

        ### SAMPLING ###
        C = sample_X
        n_obj=1000
        data_points = self.number_points
        n_begin = C.shape[0]        
        C1 = C.sample(n=data_points,replace=False,)        
        sub_num = int(data_points/(C.shape[1]*2))

        
        for c in C:                                         
            min_val = C[c].min()
            max_val = C[c].max()
            interval = max_val-min_val
            cur_sub_min = C[C[c]<min_val+0.1*interval].sample(n=sub_num,replace=True)    #As you can see we are taking values on the extreme parts of the distribution
            C1 = pd.concat([C1,cur_sub_min],axis=0)
            curr_sub_max = C[C[c]>max_val-0.1*interval].sample(n=sub_num,replace=True)
            C1 = pd.concat([C1,curr_sub_max],axis=0)

        
        C1 = C1.drop_duplicates()
        C1 = C1.sample(n=data_points,replace = False)
        
        
        subX = C1
        sub_list_Y = list(map(lambda df: df.loc[C1.index], samples_list_Y))   #selecting only the elements associated with the constraint subsampling.
        return subX, sub_list_Y

    def take_and_save_samples(self):

        '''
        Finally, this function is in charge of taking the samples and saving them (pretty obvious lol)
        '''

        X, Y_list = self.sample() 
        X.to_csv(self.path_X + '.csv', sep = "\t")
        for i_org, path_organism in enumerate(self.paths_Y):
            Y_list[i_org].to_csv(path_organism + '.csv', sep = "\t")


if __name__ == "__main__":

    MyFavoriteSubsampler = Subsampler()
    MyFavoriteSubsampler.take_and_save_samples()

