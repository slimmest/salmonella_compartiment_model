USAGE
=====

Installation
------------
To use the scripts create a virtual environment with python 3.8 and install things as needed

.. code-block:: console

    conda create --name py38 python=3.8	

Codes
-----
.. autofunction:: dfba.Run
.. autofunction:: batch_dfba.batch
.. autoclass:: ode_experiment.OdeExperimentSpec
   :members:
.. autoclass:: hyperparameter_selection.HyperParameterSelection
   :members:
