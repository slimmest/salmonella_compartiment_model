.. Compartiment Model documentation master file, created by
   sphinx-quickstart on Tue Apr  4 15:32:29 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Compartiment Model's documentation!
==============================================
**Salmonella Compartiment Model** Simulation of a dynamic systems representing the infection of Salmonella in a healthy human gut.

.. note::
   This documentation is under developement. 
.. toctree::
   usage
   developement
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
