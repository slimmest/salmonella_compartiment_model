import numpy as np
#returns a function 


def speed_map(x,flux,parameters): 
    #update(parameters)

    '''
    This part develops the whole ode system that will be used in the other files.
    '''
    state_index = parameters['state_index']
    rho = parameters['rho']
    alpha = parameters['alpha']
    D = parameters['D']
    beta = parameters['beta']
    gamma = parameters['gamma']
    s_in = parameters['s_in']
    K_s = parameters['K_s']
    L = parameters['L']
    death = parameters['death']
    C_but = parameters['C_but']
    number_state = len(parameters['state_variable'])
    dx = np.zeros(number_state)
    def VF(N):
        if N>parameters["threshold_sth"]:
            return parameters['virulence_factor']
        else: 
            return 0
    gamma_oxygen = gamma['O2']
    #luminal compartment
    dx[state_index['Styphi']] = x[state_index['Styphi']]*(flux['Styphi'][state_index['Styphi']]-rho*x[state_index['neutrophils']]-D['Styphi'])
    dx[state_index['Fprau']] = x[state_index['Fprau']]*(flux['Fprau'][state_index['Fprau']]-rho*x[state_index['neutrophils']]
            -alpha*x[state_index['O2']]/(x[state_index['O2']] + K_s['O2'])-D['Fprau'])
    dx[state_index['O2']] = gamma_oxygen*(x[state_index['O2_e']]
            - x[state_index['O2']])-x[state_index['O2']]*sum(beta[i]*x[state_index[i]] for i in parameters['reduced_molecules']) + x[state_index['Styphi']]*flux['Styphi'][state_index['O2']] -D['O2']*x[state_index['O2']]
    dx[state_index['nitric_oxide']] =  x[state_index['Styphi']]*flux['Styphi'][state_index['nitric_oxide']]+ gamma['nitric_oxide']*(x[state_index['nitric_oxide_e']]- x[state_index['nitric_oxide']]) - beta['nitric_oxide']*x[state_index['nitric_oxide']]*x[state_index['O2']] -D['nitric_oxide']*x[state_index['nitric_oxide']]
    dx[state_index['glucose']] = flux['Fprau'][state_index['glucose']]*x[state_index['Fprau']] + flux['Styphi'][state_index['glucose']]*x[state_index['Styphi']]  -   beta['glucose']*x[state_index['glucose']]*x[state_index['O2']]+ D['glucose']*(s_in['glucose'] -x[state_index['glucose']])
    dx[state_index['galactose']] = flux['Fprau'][state_index['galactose']]*x[state_index['Fprau']]+ flux['Styphi'][state_index['galactose']]*x[state_index['Styphi']]  - beta['galactose']*x[state_index['galactose']]*x[state_index['O2']] + D['galactose']*(s_in['galactose'] -x[state_index['galactose']])
    dx[state_index['thiosulfate']] = flux['Styphi'][state_index['thiosulfate']]*x[state_index['Styphi']] - beta['thiosulfate']*x[state_index['thiosulfate']]*x[state_index['O2']] + D['thiosulfate']*(s_in['thiosulfate'] -x[state_index['thiosulfate']])
    dx[state_index['galactarate']] = flux['Styphi'][state_index['galactarate']]*x[state_index['Styphi']]  + beta['galactose']*x[state_index['galactose']]*x[state_index['O2']] - D['galactarate']*x[state_index['galactarate']]
    dx[state_index['glucarate']] = flux['Styphi'][state_index['glucarate']]*x[state_index['Styphi']]  + beta['glucose']*x[state_index['glucose']]*x[state_index['O2']] - D['glucarate']*x[state_index['glucarate']]
    dx[state_index['nitrate']] = flux['Styphi'][state_index['nitrate']]*x[state_index['Styphi']]  + beta['nitric_oxide']*x[state_index['nitric_oxide']]*x[state_index['O2']] - D['nitrate']*x[state_index['nitrate']]
    dx[state_index['tetrathionate']] = flux['Styphi'][state_index['tetrathionate']]*x[state_index['Styphi']]  + beta['thiosulfate']*x[state_index['thiosulfate']]*x[state_index['O2']] - D['tetrathionate']*x[state_index['tetrathionate']]
    dx[state_index['butyrate']] = flux['Fprau'][state_index['butyrate']]*x[state_index['Fprau']] + gamma['butyrate']*(x[state_index['butyrate_e']]-x[state_index['butyrate']]) -D['butyrate']*x[state_index['butyrate']]
    dx[state_index['neutrophils']] = gamma['neutrophils']*(x[state_index['neutrophils_e']]-x[state_index['neutrophils']])-(D['neutrophils'] + death['neutrophils'])*x[state_index['neutrophils']]
    but_trigger = x[state_index['butyrate_e']]/(x[state_index['butyrate_e']]+ K_s['butyrate'])
    #Epitelial compartment (the one we must change)
    dx[state_index['nitric_oxide_e']] = C_but['nitric_oxide']*x[state_index['nitric_oxide_e']]*(L['nitric_oxide']-x[state_index['nitric_oxide_e']])*(x[state_index['nitric_oxide_e']]-but_trigger*L['nitric_oxide']) - death['nitric_oxide']*x[state_index['nitric_oxide_e']] + gamma['nitric_oxide']*(x[state_index['nitric_oxide']]-x[state_index['nitric_oxide_e']]) + VF(x[state_index['Styphi']])
    dx[state_index['O2_e']] = L['O2']+ gamma_oxygen*(x[state_index['O2']]-x[state_index['O2_e']]) - beta['butyrate_e']*x[state_index['O2_e']]*x[state_index['butyrate_e']] -death['O2']*x[state_index['O2_e']]
    dx[state_index['neutrophils_e']] =  C_but['neutrophils']*x[state_index['neutrophils_e']]*(L['neutrophils_e']-x[state_index['neutrophils_e']])*(x[state_index['neutrophils_e']]-but_trigger*L['neutrophils_e'])- death['neutrophils']*x[state_index['neutrophils_e']] + gamma['neutrophils']*(x[state_index['neutrophils']] -x[state_index['neutrophils_e']]) +VF(x[state_index['Styphi']])
    dx[state_index['butyrate_e']] = -beta['butyrate_e']*x[state_index['O2_e']]*x[state_index['butyrate_e']] + gamma['butyrate']*(x[state_index['butyrate']]-x[state_index['butyrate_e']])
    return dx
