from dfba_sampling.metamodel import flux_metamodel_Wrapper
from ode_experiment import OdeExperimentSpec
from dfba_sampling.metamodel import Metamodel   #once we have the data points is dfba_sampling who generates the metamodel (that's why there's no script here which can generate the model in this folder)
from dfba import Run
import yaml
import numpy as np
#import sys
#import argparse

class MetamodelTester():
    ''' This class allows to test the metamodels computed in 'hyperparameters' module which were computed for different hyperparameter values (in this case: mu values). 
    At the end the output is a simulation generated with 'dfba' module with the metamodel. It is necessary to clarify that for each metabolite there is one metamodel. Maybe all of those metamodels are the same (they diverge depending which hyperparameters they have) or they could all different. This information is stored in a mu_dictinary file which is necesary to have for running this class 
    '''


    def __init__(self, paths,number_obs,iter_max, max_order, selector):
        self.config_ODE = paths['config_ODE']
        self.output_folder = paths['output_folder']
        self.config_exp = paths['config_exp']
        self.path_to_constraints = paths['path_to_constraints']
        self.path_to_fluxes = paths['path_to_fluxes']
        self.path_to_metamodel_folder = paths['path_to_metamodel_folder']
        self.path_to_mu = paths['path_to_mu']
        self.nobs = number_obs
        self.iter_max = iter_max
        self.max_order = max_order
        self.dict_mm = {}
        self.SELECTED = selector['SELECTED']
        self.SELECTED_SAVE = selector['SELECT_SAVE']    
    
    def create_path_structs(self):
        '''Just creating correctly the path for the rest of the code'''
        self.bact = [path.split('_')[-1].split('.')[0].capitalize() for path in self.path_to_fluxes]

    def read_mu_files(self, mu_forced_value = None):
        '''Reading mu files from mu dictionary, also we can force only one value of mu (one metamodel) for each metabolite (useful when testing rapid ideas)'''
        try:
            with open(self.path_to_mu,'r') as f:
                mu_dict = yaml.safe_load(f)
                self.mu = mu_dict
            if mu_forced_value != None:
                for key in self.mu.keys():
                    self.mu[key] = mu_forced_value
        except:
            print("Probably there's no mu dict")
    
    def open_metamodel_vessel(self):
        '''we open a new ode experiment, in which we are going to run dfba simulation using the recently created and fitted metamodels.'''
        Ode = OdeExperimentSpec(config_ODE=self.config_ODE,config_exp=self.config_exp)
        self.parameters = Ode.param
        exp = Ode.exp 
        self.mm = [Metamodel(kernel_type="Matern", bact=self.bact[i],exp=exp)            
                                for i in range(len(self.path_to_fluxes))
                                ]
    def load_metamodel_data(self):
        '''
        We are going to load all the information of the metamodels created for each mu in the 'metamodel vessel' created in the method above. In that way we can then store each metamodel for each metabolite (reagarding the information of mu dictionary) and create a file which will be loaded into the dfba simulation.
        '''
        for i in range(len(self.path_to_fluxes)):   ## This goes through each organism
            s = self.bact[i]
            self.mm[i].new_constraintlearningset(self.path_to_constraints, n_obs=self.nobs, index_col=0)
            self.mm[i].new_fluxlearningset(self.path_to_fluxes[i], n_obs=self.nobs, index_col=0)    
            self.mm[i].P_MaxOrder(self.max_order)
            if self.SELECTED:
                selected_file = f'data/selected_metamodel_species_{s.lower()}_results.npz'    
                self.mm[i].load_attributes(theta= selected_file)
                if self.SELECT_SAVE:
                    self.mm[i].save_results(f'data/selected_metamodel_species_{s.lower()}_results.npz')
            else:
                curr_mu = {k : self.path_to_metamodel_folder+f"{self.iter_max}/mu_{self.mu[s][k]}/species_{s.lower()}_results.npz"  for k in self.mu[s].keys() }
                self.mm[i].load_attributes(theta= curr_mu)
                self.mm[i].save_results(f'data/selected_metamodel_species_{s.lower()}_results.npz')
            self.dict_mm[s.capitalize()] = self.mm[i]

    def run_simulation(self):
        '''
        With the metamodels ready to go we just click run on the dfba model and wait for the results
        '''    

        TY_metamodel = Run(config_exp=self.config_exp, RHS='flux_metamodel_Wrapper', dict_mm=self.dict_mm, output=self.output_folder)  #Maybe we can try to change the RHS parameter
        return TY_metamodel
        
    def save_meta_simulated_results(self, metamodel_results):   #Im not so sure what is being saved here, because in Run the files are already saved.
        TY_metamodel = metamodel_results
        TY = TY_metamodel.drop(columns='t').values
        np.savez_compressed('data/test_metamodel.npz',Y_mm=TY)
    
    def main(self, mu_forced_value= None):
        '''
        The pipeline for the testing.
        '''
        self.create_path_structs()
        self.read_mu_files(mu_forced_value)
        self.open_metamodel_vessel()
        self.load_metamodel_data()
        meta_results = self.run_simulation()
        #self.save_meta_simulated_results(meta_results), I dont understand quite well where this is going to.


        
 



if __name__ == '__main__':
    
    #For the momemt no parsing needed.

    max_order = 2
    iter_max = 150
    number_obs = 100 #this should come from the hyperparameters.py file

    selector = {

        'SELECTED': False,
        'SELECT_SAVE': False
    }

    paths = {
        'config_ODE': "data/EDO_parameters.yml",
        'output_folder': 'data/test/sample_automatized_model/',
        'config_exp': "data/dFBA_styphi_fprau_oxygen_parameter.yml",
        'path_to_constraints': "data/samples/database_subsample_1999_bounds_constraints.csv",
        'path_to_fluxes': ["data/samples/database_subsample_1999_bounds_fluxes_Styphi.csv", "data/samples/database_subsample_1999_bounds_fluxes_Fprau.csv"],
        'path_to_metamodel_folder':  'data/samples/hyperparameter_selection_sqrt_KV/',
        'path_to_mu': 'data/samples/observation_selection/mu_dictionary.pickle' 

    }

    MyFavoriteMetamodelTester = MetamodelTester(paths,number_obs,iter_max, max_order, selector)
    MyFavoriteMetamodelTester.main()

