import argparse
from dfba_sampling.constraint_sampler import Constraint_Sampler
from dfba import Run
from mpi4py import MPI
import numpy as np
import multiprocessing as mp
c_mpi4py = MPI.COMM_WORLD

MPIRUN = False #Script launched with MPI (turn on for code testing on laptop. Otherwise, code is sequential for arrayjob on a cluster : one job per parameter set in parameter space exploration)

def batch(sample_config, config_experiment, output_folder, config_ode):

    '''
    This function allows to run multiple times the ODE system with the FBA models in it, and also load some ode parameters. It's neccesary for the sampling proceadure and the training of the model.
    The sampling data is stored in files inside 'data' folder. The file which its path is given in 'sample config' variable contains the parameter N_y0 which determines how many samples will be taken for this batch experiment.
    '''
    cs = Constraint_Sampler(
        sample_config = sample_config,
        exp_config = config_experiment   #This "constraint sampler" is in charge of sampling the domain, that is all the constraint from the constraint space which is reduced to the subespace close to the ODE's valid solutions domain.
    )
    cs.sample_yaml_files()
    if MPIRUN:
        Njob=c_mpi4py.size
        jobID=c_mpi4py.rank
    else:                 
        jobID=0                     # it is not an array job => one unique sequential job
        Njob= mp.cpu_count()-1
   
    nb_jobs = cs.param['N_y0']  #set in 5
    cs.sample_yaml_files
    shunk = nb_jobs//Njob
    Exp_index={}
    for i in np.arange(nb_jobs%Njob):
        Exp_index[i]={'min':i*(shunk+1),'max':min(nb_jobs,(i+1)*(shunk+1))}
    for i in np.arange(nb_jobs%Njob,Njob):
        Exp_index[i] = {'min':nb_jobs%Njob+i*(shunk),'max':min(nb_jobs,nb_jobs%Njob+(i+1)*(shunk))}
    print("Exp_index",Exp_index)
    params = [] 

    for i_exp in np.arange(nb_jobs):
        if i_exp>=Exp_index[jobID]['min'] and i_exp<Exp_index[jobID]['max']:
            print('\n\n********************************************************************\n')
            print('************** rank :', i_exp)
            print('********************************************************************\n')
        params.append((config_ode, output_folder+f"sample_{i_exp}/config.yml", output_folder+f"sample_{i_exp}/"))  ## Here we take the space sampling. 
        #params.append((args.config_ode, 'data/test/' +f"sample_{i_exp}/config.yml", args.output_folder+f"sample_{i_exp}/"))
    # Now we run in array jobs:     
    print('Using ' + str(mp.cpu_count()) + ' units for processing')
    pool = mp.Pool(mp.cpu_count())
    # Since there's no necessary communication between cpus, we run this asynchronically. 
    results = pool.starmap_async(Run, [param for param in params]).get()    ##here, the Run function from 'dfba.py' is used to make multiple iterations of the system
    
    pool.close()
    pool.join() 
    return nb_jobs

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Command description.")
    parser.add_argument(
        "-ode",
        "--config_ode",
        default='data/EDO_parameters.yml',
        help="ode parameters: state variable, parameter of the general system (YAML)",
        type=str,
    )   
    parser.add_argument(
        "-s",
        "--sample_config",
        default='data/sampling_parameters.yml',
        help="sampling parameters, output paths (YAML)",   #Aparentemente este archivo de configuraciòn nos gustaria tenerlo igual para todas las muestras
        type=str,
    )
    parser.add_argument(
        "-o",
        "--output_folder",
        help="folder for the ode output",
        default='data/samples/',
        type=str,
    )
    parser.add_argument(
        "-exp",
        "--config_experiment",
        help="folder for the ode output",
        default= 'data/dFBA_styphi_fprau_oxygen_parameter.yml',
        type=str,
    )
    
    args = parser.parse_args()

    batch(sample_config = args.sample_config,
           config_experiment = args.config_experiment,
             output_folder = args.output_folder,
               config_ode = args.config_ode)
    
