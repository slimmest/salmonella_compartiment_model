# Salmonella_compartiment_model

Differential equations simulation representing Salmonella infection
Uses dfba_sampling class

## **Model and sampling functions**:

`ode_system`:  It has the whole ODE system.

`ode_experiment`: It has two main functions, the first one is in charge of _setting all the conditions_ for solving the ODE and the second one does the _time integration_ steps to solve the ode.

`dfba`: `ode_system` and `ode_experiment` are used to generate one 'run' of the simulation. FBA computation can be done either with classical approach or with a surrogate model. 

`batch_dfba`: Using dfba sampling, it takes samples from the ode system to generate the data for the metamodel.

`hyperparameter_selection`: It is in charge of testing different combinations of the _lasso/mu/regularization_ ($\mu$) hyperparameter. Also it plots them.

`observation_selection_speed_up`: It is in charge of testing different combinations of the _number of observations_ ($\N_{obs}$) hyperparameter with a defined value of $\mu$. Also it plots them

`subsampling`: It makes a subsamples from the samples taken after `batch_dfba`. The subsamples are stored in .csv files

## **Testing functions**

`Ode_metamodel`: It's a test of the metamodel.

`run_FBA`



`test_file`

`test_ode_system`

## **Plotting functions**

`plot_comparision`

`plot_dfba_backup`

`plot_dfba`

`qq_plot`
