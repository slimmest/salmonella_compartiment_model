from dfba_sampling.experiment import ExperimentSpec
from dfba_sampling.FBA import FBA_model_Wrapper
from dfba_sampling.dFBA import Constraint_definition
import numpy as np
import scipy as sc
import ode_system

parameters = {}  
parameters['state_variable'] = ['Styphi', 'Fprau', 'O2', 'NO',
        'glucose', 'galactose', 'thiosulfate', 'galactarate', 'glucarate', 'nitrate',
        'tetrathionate', 'butyrate', 'neutrophils', 'NO_e', 'O2_e', 'neutrophils_e',
        'butyrate_e']
parameters['state_index'] = {element: i_elements for i_elements,element in enumerate(parameters['state_variable'])}
parameters['rho'] = 0.1
parameters['alpha'] = 1
parameters['K_o2'] = 0.1
parameters['gamma_o2'] = 0.1
parameters['beta']= np.ones(17)*0.1
parameters['gamma_NO'] = 0.1
parameters['gamma_but'] = 0.1
parameters['source'] = np.ones(7)*0.1
parameters['gamma_N'] = 0.1
parameters['d_N'] = 0.1
parameters['nu'] = 1
parameters['K'] = 0.1
parameters['L_No'] = 0.1
parameters['L_n'] = 0.1
parameters['L_o2'] = 0.1
parameters['d_o2'] = 0.1
parameters['d_NO'] = 0.1
parameters['d_thio'] = 0.1
parameters['C_NO'] = 1
parameters['C_O2'] = 1
parameters['C_n'] = 1
parameters['threshold_sth'] = 0.2
parameters['virulence_factor'] = 0.5

flux_fba = [1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1]
y0 = 0.1*np.ones(17)
time = [0, 5]

def ode_function(t,x):
    return ode_system.speed_map(x,flux_fba,parameters)
""" print(y0)
test = ode_function(1,y0)
print(test) """

sol = sc.integrate.solve_ivp(ode_function, time, y0) 

import matplotlib.pyplot as plt
T = sol.t
Y = sol.y
luminal_results_bact_neut = Y[[0, 1, 12],:]
luminal_results_rest = Y[2:12,:]
epithelial_results =  Y[13:,:]
fig, axs = plt.subplots(1,2) 
ax2 = axs[0].twinx()
axs[0].plot(sol.t, luminal_results_rest.T)
axs[0].set_xlabel('Time [days]')
axs[0].set_ylabel('Concentration [µmol/L]')
axs[0].legend(['O2','NO','glucose','galactose','Thiosulfate','Galactarate','glucarate','Nitrate','tetrathionate','butyrate','neutrophils'])
axs[0].set_title('Luminal Compartment')
ax2.plot(sol.t, luminal_results_bact_neut.T, linestyle='--')
ax2.set_ylabel('Bacterial and neutrophils concentration [g/L]')
ax2.legend(['Styphi','Fprau', "neutrophils"], loc = 'upper right')
axs[1].plot(sol.t, epithelial_results.T)
axs[1].set_xlabel('Time [days]')
axs[1].set_ylabel('Concentration [µmol/L]')
axs[1].legend(['NO','O2','neutrophils','butyrate'])
axs[1].set_title('Epithelial Compartment') 
plt.show()
