import numpy as np

# loading file obtained after mu assembling
bact = ['Fprau','Styphi']
selected = {b : np.load(f'data/selected_metamodel_species_{b.lower()}_results.npz') for b in bact}

#loading file obtained with n_obs = 400 points that should be identical with the previous theta
obs = {b: np.load(f'data/samples/observation_selection/150/n_obs_400/species_{b.lower()}_results.npz') for b in bact}

for b in bact:
    print('differences theta : ',b,np.linalg.norm(selected[b]['theta']-obs[b]['theta']))
    print('differences theta_0 : ',b,np.linalg.norm(selected[b]['theta_0']-obs[b]['theta_0']))


