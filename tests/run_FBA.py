from dfba_sampling.experiment import ExperimentSpec
from dfba_sampling.FBA import FBA_model_Wrapper
from dfba_sampling.dFBA import Constraint_definition
import numpy as np

rng = np.random.default_rng(42) 
exp = ExperimentSpec(config="data/dFBA_styphi_fprau_oxygen_parameter.yml",
                     networks=["data/Salmonella_FBA.xml", "data/Fprau_FBA.xml"])


y = np.append(rng.uniform(0, 10, len(exp.substrate_metabolites)+len(exp.output_metabolites)),
                   rng.uniform(0, 1, len(exp.species)))
constraint = Constraint_definition(y,exp)
print(y)
print(exp.species)

for i in exp.species:
    optimal_flux, flag_err = FBA_model_Wrapper(constraint,i,exp)
    print(optimal_flux)