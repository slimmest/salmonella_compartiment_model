# -*- coding: utf-8 -*-

import os
from pathlib import Path  # noqa: F401


from dfba_sampling.experiment import ExperimentSpec
import numpy as np
import pandas as pd
import yaml

class OdeExperimentSpec(object):
    ''' The object OdeExperimentSpec holds all the information relating to the metabolic networks, and state variables as well as the in    integration procedure'''
    def __init__(self, config_ODE=None, config_exp = None):
        
        if config_ODE is not None:
            with open(config_ODE,'r') as stream:
                param = yaml.safe_load(stream)
            
            for k in param.keys():
                if type(param[k]) is dict:
                    print(param[k])
                    if 'all' in param[k].keys():
                        print(k)
                        not_equal_to_all = {}
                        for tmp_k in param[k].keys():
                            if tmp_k.upper() != 'ALL':
                                not_equal_to_all[tmp_k] = param[k][tmp_k]
                        for field in param['state_variable']:
                            param[k][field] = param[k]['all']
                        for field in not_equal_to_all.keys():
                            param[k][field] = not_equal_to_all[field]
            param['state_index'] = {element: i_elements for i_elements,element in enumerate(param['state_variable'])}
            param['map_oxidized_to_reduced'] = {oxi: red for oxi,red in zip(param['oxidized_molecules'],param['reduced_molecules'])}
                        
            self.param = param
            self.config = config_ODE
            
        if config_exp is not None:
            self.exp = ExperimentSpec(config=config_exp)
            ## transfers vector from state_index numbering to FBA_index numbering: iterate compound_name (i.e. FBA_numbering) and extract the corresponding compound id in the ODE state space.
            self.param['state_index_to_FBA_index']=[self.param['state_index'][i] for i in self.exp.compound_name]
            
            for specie in self.exp.species:
                self.param[specie+'_compounds']=list(self.exp._species[specie]['Substrate_Reaction_Map'])+list(self.exp._species[specie]['Output_Reaction_Map']) + [specie]
            self.param['fba_index'] = {
                                        specie : [self.param['state_index'][i] for i in self.param[specie+'_compounds']]
                                        for specie in self.exp.species
                                      }

            self.param['initial_value']={
                                        value:self.exp.__dict__[field][value]['initial_value']
                                            for field in ['_species','_substrate_metabolites','_output_metabolites']
                                            for value in self.exp.__dict__[field].keys()
                                        }



    def TimeIntegrate(self,ode_function,time_range,y0,eps=1e-15):
        ''' Order 1 semi-implicit euler scheme '''


        nrows = int((time_range[1]-time_range[0])/self.exp.time_step)
        nb_species = len(self.exp.species)
        nb_compound = len(self.exp.compound_name)
        ncols = 1 + len(self.param['state_variable'])  # first column stands for the time.
        ncols_constraints = len(self.exp.substrate_metabolites)
        ncols_flux = {i : len(self.exp.compound_name) for i in self.exp.species}

        y = y0.copy()
        result_y = np.zeros([nrows, ncols])
        result_y[0, 1:] = y
        result_y[0,0] = time_range[0]
        result_constraint = np.zeros([nrows, ncols_constraints])
        result_flux = {i : np.zeros([nrows, ncols_flux[i]]) for i in self.exp.species}

        for i in range(nrows):

            t = time_range[0]+ i * self.exp.time_step
            t_dfba = time_range[0]+ (i + 1) * self.exp.time_step  # dFBA starts at .01 in testfiles...
            if i % 100 == 0:
                print("t=", t, "/", time_range[1], "hours")            
            dy,flux_fba,constraint = ode_function(t,y)
            for j in self.exp.species:
                result_flux[j][i, :] = flux_fba[j]
            result_constraint[i, :] = constraint
            id_pos = dy >= 0.0
            id_neg = dy < 0.0
            y[id_neg] = y[id_neg] / (
                1 - self.exp.time_step * dy[id_neg] / (y[id_neg]+eps)
            )
            y[id_pos] = y[id_pos] + self.exp.time_step * dy[id_pos]

            if i < nrows - 1:
                result_y[i + 1] = np.append(t_dfba, y)

        return result_y, result_flux, result_constraint


