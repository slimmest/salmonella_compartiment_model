import pandas as pd
from dfba_sampling.metamodel import Metamodel
from dfba_sampling.metamodel import flux_metamodel_Wrapper
from pathlib import Path
from mpi4py import MPI
import numpy as np
import numpy.linalg as npl
import argparse
from ode_experiment import OdeExperimentSpec
import matplotlib.lines as mlines
c_mpi4py = MPI.COMM_WORLD
import matplotlib.pyplot as plt
import yaml
from dfba import Run
import time
import matplotlib.lines as mlines


'''
I believe this makes the same as the 'hyperparameter selection'm because it takes constraint data points and also manages to simulate de ODE, thus making the model to learn. 
'''

class NbObservationSelection(object):
    '''Class for selecting the number of points in the RKHS setting'''

    def __init__(self, path_to_constraints=None, path_to_fluxes = None, n_obs=[10,20,30,40,50,60,70,80,90,100,200,300,400,500,600,700,800,900,1000], mu_dict=None, output = 'data/samples/observation_selection/', iter_max=100, config_ODE='data/EDO_parameters.yml',config_exp="data/dFBA_styphi_fprau_oxygen_parameter.yml", max_order=3):
        self.output = output
        self.n_observations =n_obs  #remember this is one of the hyperparameter we want to know its optimal value.
        self.mu_dict=mu_dict        #this is the other one 
        self.iter_max = iter_max
        self.path_to_constraints=path_to_constraints
        self.path_to_fluxes = path_to_fluxes
        self.bact=[path.split('_')[-1].split('.')[0].capitalize() for path in self.path_to_fluxes]
        self.config_ODE=config_ODE
        self.NewOde(config_exp)
        self.mm = [Metamodel(kernel_type="Matern", bact=self.bact[i],exp=self.exp) 
                          for i in range(len(path_to_fluxes))
                          ]
        self.max_order=max_order

    def NewOde(self, config_exp):
        Ode = OdeExperimentSpec(config_ODE=self.config_ODE,config_exp=config_exp)
        self.parameters = Ode.param        
        self.exp = Ode.exp 
        
    def JobChunks(self, n_observations = None, MPIRUN=True):
        
        if MPIRUN:
            Njob=c_mpi4py.size
            jobID=c_mpi4py.rank
        else:                 
            try:
                jobID=int(os.environ['SLURM_ARRAY_TASK_ID'])-1 # if array job in slurm with slurm scheduler
                Njob=int(os.environ['SLURM_ARRAY_TASK_COUNT'])
            except:
                jobID=0                     # it is not an array job => one unique sequential job
                Njob=1
           
        nb_jobs = n_observations
           
        shunk = nb_jobs//Njob
        Exp_index={}
        for i in np.arange(nb_jobs%Njob):
            Exp_index[i]={'min':i*(shunk+1),'max':min(nb_jobs,(i+1)*(shunk+1))}
        for i in np.arange(nb_jobs%Njob,Njob):
            Exp_index[i] = {'min':nb_jobs%Njob+i*(shunk),'max':min(nb_jobs,nb_jobs%Njob+(i+1)*(shunk))}
        print("Exp_index",Exp_index)https://www.google.com/search?channel=fs&client=ubuntu&q=corrida+translat

    def ComputeMetamodelHyperparameterGrid(self, MPIRUN=True):
        n_observations=self.n_observations
        Exp_index, jobID, Njob = self.JobChunks(n_observations=len(self.n_observations), MPIRUN = MPIRUN)


        for i_n_obs, n_obs in enumerate(n_observations):
            if i_n_obs>=Exp_index[jobID]['min'] and i_n_obs<Exp_index[jobID]['max']:
                for i in range(len(self.path_to_fluxes)):
                    self.mm[i].new_constraintlearningset(self.path_to_constraints, n_obs=n_obs, index_col=0)
                    self.mm[i].new_fluxlearningset(self.path_to_fluxes[i], n_obs=n_obs, index_col=0)    

                    self.mm[i].P_MaxOrder(self.max_order)
                    self.mm[i].GramMatrix()
            
                iter_max = self.iter_max
                eps_res = 1e-4
                learning_rate = 1e-5

                for i in range(len(path_to_fluxes)):
                    self.mm[i].learn_metamodel(
                            mu=self.mu_dict,
                            iter_max=iter_max,
                            eps_res=eps_res,
                            flag_print=True,
                            learning_rate=learning_rate,
                        )

                    s=self.bact[i]
                    Path(self.output+f"{iter_max}/n_obs_{n_obs}/").mkdir(parents=True, exist_ok=True)
                    self.mm[i].save_results(self.output+f"{iter_max}/n_obs_{n_obs}/species_{s.lower()}_results.npz")


    def PlotLossCompute(self, nb_repetitions=5, comps_output_base='data/samples/observation_selection/', MPIRUN = True):
        """Lasso path"""
        iter_max=self.iter_max
        nmodels = self.n_observations
        n_it = nb_repetitions
        tps_dFBA = np.zeros(n_it)
        tps_mm = np.zeros([n_it, len(nmodels)])
        norms = np.zeros([n_it, len(nmodels)])
        Y_fba = [None for i_t in range(nb_repetitions)]
        Y_mm = [[None for n in nmodels] for i_t in range(nb_repetitions)]        
        Exp_index, jobID, Njob = self.JobChunks(n_observations=n_it, MPIRUN = MPIRUN)
        comps_output=comps_output_base+f"computation_{jobID}_speedup.npz"
        for i_t in range(n_it):
            if i_t>=Exp_index[jobID]['min'] and i_t<Exp_index[jobID]['max']:
                input_file = f'data/samples/sample_{i_t}/config.yml'
                self.NewOde(input_file)
                debut = time.time()
                TY = Run(config_exp=input_file)
                Y_fba[i_t] = TY.drop(columns='t').values
                fin = time.time()
                tps_dFBA[i_t] = fin - debut
                for j, n in enumerate(nmodels):
                    dict_mm={}
                    self.mm = [Metamodel(kernel_type="Matern", bact=self.bact[i],exp=self.exp) 
                          for i in range(len(path_to_fluxes))
                          ]
                    for i in range(len(self.path_to_fluxes)):
                        s = self.bact[i]
                        self.mm[i].new_constraintlearningset(self.path_to_constraints, n_obs=n, index_col=0)
                        self.mm[i].new_fluxlearningset(self.path_to_fluxes[i], n_obs=n, index_col=0)    
                        self.mm[i].P_MaxOrder(self.max_order)
                        self.mm[i].load_attributes(theta=self.output+f"{iter_max}/n_obs_{n}/species_{s.lower()}_results.npz")
                        dict_mm[s.capitalize()] = self.mm[i]
                    debut = time.time()
                    TY = Run(config_exp=input_file, RHS=flux_metamodel_Wrapper, dict_mm=dict_mm)
                    Y_mm[i_t][j] = TY.drop(columns='t').values
                    fin = time.time()
                    tps_mm[i_t,j] = fin - debut
                    norms[i_t, j] = np.linalg.norm(Y_mm[i_t][j] -  Y_fba[i_t]) / np.linalg.norm(Y_fba[i_t])
                    print(10*'*'+' current norm : '+str(norms[i_t,j]))
                np.savez_compressed(comps_output, norms=norms,tps_dFBA=tps_dFBA, tps_mm=tps_mm, Y_fba=Y_fba[i_t],Y_mm = Y_mm[i_t] )

    def PlotLossPlot(self, nb_repetitions=5, comps_output_base='data/samples/observation_selection/'):
        SMALL_SIZE = 7
        MEDIUM_SIZE = 8
        BIGGER_SIZE = 10
        font={"small":SMALL_SIZE,"medium":MEDIUM_SIZE,"big":BIGGER_SIZE}
        plt.rc('font', family='Arial',size=BIGGER_SIZE)          # controls default text sizes
        plt.rc('axes',titlesize=BIGGER_SIZE)     # fontsize of the axes title
        plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
        plt.rc('xtick',labelsize=SMALL_SIZE)    # fontsize of the tick labels
        plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
        plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
        plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
        
        unit_width = 5
        unit_height= 2.5
        nmodels = self.n_observations
        comps_output = comps_output_base+"computation_0_speedup.npz"
        comps=np.load(comps_output)
        tps_dFBA=comps['tps_dFBA']
        tps_mm=comps['tps_mm']
        norms=comps['norms']
        for i in range(1,nb_repetitions):
            comps_output = comps_output_base+f"computation_{i}_speedup.npz"
            comps=np.load(comps_output)
            tps_dFBA+=comps['tps_dFBA']
            tps_mm+=comps['tps_mm']
            norms+=comps['norms']
        fig, axes = plt.subplots(1,  figsize=(2*unit_width, 2*unit_height))
        axes.plot(nmodels, (tps_dFBA[:,None] / tps_mm[:,:len(nmodels)]).mean(axis=0), marker='o', color='red', linewidth=1, markersize=7,label='speed up')
        axes.fill_between(nmodels, (tps_dFBA[:,None] / tps_mm[:,:len(nmodels)]).mean(axis=0)-(tps_dFBA[:,None] / tps_mm[:,:len(nmodels)]).std(axis=0), (tps_dFBA[:,None] / tps_mm[:,:len(nmodels)]).mean(axis=0)+(tps_dFBA[:,None] / tps_mm[:,:len(nmodels)]).std(axis=0), color='red',alpha=0.2,lw=0.2)
        axes.set_xlabel('number of basis functions used for prediction')
        axes.set_ylabel("Speedup versus dFBA", color="rhttps://www.google.com/search?channel=fs&client=ubuntu&q=corrida+translat(axis=0)-norms[:,:len(nmodels)].std(axis=0),norms[:,:len(nmodels)].mean(axis=0)+norms[:,:len(nmodels)].std(axis=0), color='darkblue', alpha=0.2, linewidth=0.2)        
        ax2.set_ylabel(r"Loss $\frac{||y - \hat{y}||_2}{||y||_2}$", rotation=-90, color='darkblue')

#        ax2.yaxis.set_label_coords(1.075,.5)
        ax2.set_yscale("log")
        blue_line = mlines.Line2D([], [], color='darkblue', marker='x',
                          markersize=7, label='relative error',lw=1)
        red_line = mlines.Line2D([], [], color='red', marker='o',
                          markersize=7, label='speed up',lw=1)                          
        axes.legend(handles=[blue_line,red_line])
        
        # adapt_save_fig_top(fig, "speedup_dFBA.pdf")
        fig.tight_layout()
        fig.savefig(self.output+'ODE_system_all_speedup.pdf')




if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Command description.")
    parser.add_argument(
        "-c",
        "--compute",
        action='store_true',
        help="compute metamodel for a grid of number of observations",
    )   
    parser.add_argument(
        "-pc",
        "--plot_compute",
        action='store_true',
        help="Computation of fba vs mm speedup for plotting",
    )
    parser.add_argument(
        "-p",
        "--plot",
        action='store_true',
        help="Plot speedup and loss for a grid of number of observations",
    )
    parser.add_argument(
        "-nobs",
        "--n_obs",
        help="Number of observations",
        type=str,
        default= '[50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700]',#,800]', #900,1000,1100]',
    )    
    parser.add_argument(
        "-im",
        "--iter_max",
        help="maximal number of iterations during the learning",
        type=int,
        default=150
    )
    parser.add_argument(
        "-mo",
        "--max_order",
        help="maximal interaction order in the Hoeffding decomposition",
        type=int,
        default=2
    )       
    parser.add_argument(
        "-o",
        "--output_folder",
        help="Output folder",
        default='data/samples/observation_selection/',
        type=str,
    )
    parser.add_argument(
        "-ode",
        "--config_ode",
        help="ode parameters: state variable, parameter of the general system (YAML)",
        default='data/EDO_parameters.yml',
        type=str,
    )
    parser.add_argument(
        "-exp",
        "--config_exp",
        help="dfba parameters: initial conditions and exchange reactions (YAML)",
        default="data/dFBA_styphi_fprau_oxygen_parameter.yml",
        type=str,
    )     
    parser.add_argument(
        "-mu",
        "--mu_dictionary",
        help="dictionnary of mu, the hyperparemeter tuning the group-lasso penalty, i.e. the number of selected input variables. Is manually selected based on the lasso paths.",
        default="mu_dictionary.pickle",
        type=str,
    )     
    path_to_constraints = "data/samples/database_subsample_2000_bounds_constraints.csv"
    path_to_fluxes_styphi = "data/samples/database_subsample_2000_bounds_fluxes_Styphi.csv"
    path_to_fluxes_fprau = "data/samples/database_subsample_2000_bounds_fluxes_Fprau.csv"

    path_to_fluxes = [path_to_fluxes_styphi, path_to_fluxes_fprau] 
 
    args = parser.parse_args()
    n_obs=eval(args.n_obs)

    with open(args.output_folder + args.mu_dictionary,'r') as f:
        mu_dict= yaml.safe_load(f)


    NOS = NbObservationSelection(  path_to_constraints= path_to_constraints, 
                                    path_to_fluxes = path_to_fluxes, 
                                    mu_dict=mu_dict, 
                                    output = args.output_folder, 
                                    iter_max=args.iter_max, 
                                    n_obs=n_obs,
                                    config_ODE=args.config_ode,
                                    config_exp=args.config_exp,
                                    max_order=args.max_order,
                                    )

    if args.compute:
        NOS.ComputeMetamodelHyperparameterGrid()
    if args.plot_compute:
        NOS.PlotLossCompute()
    if args.plot:
        NOS.PlotLossPlot()
