import pandas as pd
from dfba_sampling.metamodel import Metamodel
from pathlib import Path
from mpi4py import MPI
import numpy as np
import numpy.linalg as npl
import argparse
from ode_experiment import OdeExperimentSpec
import matplotlib.lines as mlines
c_mpi4py = MPI.COMM_WORLD
import matplotlib.pyplot as plt


class HyperParameterSelection(object):
    '''This code allows to tune hyperparameters in the model and obtain specific plots for it. For example lasso parameter plots. Here is where the learning happens.
    Specifically the class recieves a list of mu values (which is the regularization parameter). Also some paths and important parameters like: number of observations (is another hyperparameter), maximal amount of iterations, max iterations and the type of weigth for regularization
    ''' 


    def __init__(self, path_to_constraints=None, path_to_fluxes = None, regs=[0.0, .001,.01, .05, .1, .2, .3, .4, .5, .6, .75, 1.], output = 'data/samples/hyperparameter_selection/', n_obs=500, iter_max=100, config_ODE='data/EDO_parameters.yml',config_exp="data/dFBA_styphi_fprau_oxygen_parameter.yml", max_order=3, type_weight="identity"):
        self.regs=regs       #we only change regs (= mu)
        self.output = output
        self.n_obs = n_obs   #nobs is fixed
        self.iter_max = iter_max
        self.path_to_constraints=path_to_constraints
        self.path_to_fluxes = path_to_fluxes
        self.bact=[path.split('_')[-1].split('.')[0].capitalize() for path in self.path_to_fluxes]
        Ode = OdeExperimentSpec(config_ODE=config_ODE,config_exp=config_exp)
        self.parameters = Ode.param
        self.exp = Ode.exp        
        self.mm = [Metamodel(kernel_type="Matern", bact=self.bact[i],exp=self.exp) 
                          for i in range(len(path_to_fluxes))
                          ]
        self.max_order=max_order
        self.type_weight = type_weight

    def ComputeMetamodelHyperparameterGrid(self, MPIRUN=True):

        #Script launched with MPI (turn on for code testing on laptop. Otherwise, code is sequential for arrayjob on a cluster : one job per parameter set in parameter space exploration)

        

        if MPIRUN:
            Njob=c_mpi4py.size
            jobID=c_mpi4py.rank
        else:                 
            try:
                jobID=int(os.environ['SLURM_ARRAY_TASK_ID'])-1 # if array job in slurm with slurm scheduler
                Njob=int(os.environ['SLURM_ARRAY_TASK_COUNT'])
            except:
                jobID=0                     # it is not an array job => one unique sequential job
                Njob=1
           
        nb_jobs = len(self.regs)
           
        shunk = nb_jobs//Njob
        Exp_index={}
        for i in np.arange(nb_jobs%Njob):
            Exp_index[i]={'min':i*(shunk+1),'max':min(nb_jobs,(i+1)*(shunk+1))}
        for i in np.arange(nb_jobs%Njob,Njob):
            Exp_index[i] = {'min':nb_jobs%Njob+i*(shunk),'max':min(nb_jobs,nb_jobs%Njob+(i+1)*(shunk))}
        print("Exp_index",Exp_index)

        

        for i in range(len(self.path_to_fluxes)):
            self.mm[i].new_constraintlearningset(self.path_to_constraints, n_obs=self.n_obs, index_col=0)
            self.mm[i].new_fluxlearningset(self.path_to_fluxes[i], n_obs=self.n_obs, index_col=0)    

            self.mm[i].P_MaxOrder(self.max_order)
            
            self.mm[i].GramMatrix()



        
        for i_mu, mu in enumerate(self.regs):
        
            if i_mu>=Exp_index[jobID]['min'] and i_mu<Exp_index[jobID]['max']:
                iter_max = self.iter_max
                eps_res = 1e-1#1e-4
                learning_rate = 0.5*1e-5
                eps_sqrt=1e-6

                for i in range(len(self.path_to_fluxes)):
                    print(self.bact[i])
                    self.mm[i].learn_metamodel(
                            mu=mu,
                            iter_max=iter_max,
                            eps_res=eps_res,
                            flag_print=True,
                            learning_rate=learning_rate,
                            type_weight= self.type_weight,
                            eps_sqrt=eps_sqrt
                        )
                    
                    
                    s=self.bact[i]
                    Path(self.output+f"{iter_max}/mu_{mu}/").mkdir(parents=True, exist_ok=True)
                    self.mm[i].save_results(self.output+f"{iter_max}/mu_{mu}/nobs_{self.n_obs}_species_{s.lower()}_results.npz")


    def PlotLassoPath(self,n_test=300):
        """Lasso path"""
        iter_max=self.iter_max
        SMALL_SIZE = 7
        MEDIUM_SIZE = 8
        BIGGER_SIZE = 10
        font={"small":SMALL_SIZE,"medium":MEDIUM_SIZE,"big":BIGGER_SIZE}
        plt.rc('font', family='Arial',size=SMALL_SIZE)          # controls default text sizes
        plt.rc('axes',titlesize=MEDIUM_SIZE)     # fontsize of the axes title
        plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
        plt.rc('xtick',labelsize=SMALL_SIZE)    # fontsize of the tick labels
        plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
        plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
        plt.rc('figure', titlesize=MEDIUM_SIZE)  # fontsize of the figure title
        
        unit_width = 5
        unit_height= 2.5
        
        nregs = len(self.regs)
        constraints = pd.read_csv(self.path_to_constraints,sep='\t', index_col=0)
        fluxes={self.bact[i]: pd.read_csv(path,sep='\t',index_col=0)
                for i,path in enumerate(self.path_to_fluxes)
                }
        c_test = constraints.iloc[self.n_obs:self.n_obs+n_test]
        f_test = {k:fl.iloc[self.n_obs:self.n_obs+n_test] for k,fl in fluxes.items()}
        for i in range(len(self.path_to_fluxes)):
            self.mm[i].new_constraintlearningset(self.path_to_constraints, n_obs=self.n_obs, index_col=0)
            self.mm[i].new_fluxlearningset(self.path_to_fluxes[i], n_obs=self.n_obs, index_col=0)    
            self.mm[i].P_MaxOrder(self.max_order)

            substrate = self.mm[i].substrate_metabolites
            compound_output = self.mm[i].compound_name
            ncomp = len(compound_output)

            parts = [' & '.join(np.array(substrate)[p]) for p in self.mm[i].P]
            nparts = len(parts)
            species = self.bact[i]
            lasso_path = np.zeros([nregs, ncomp, nparts])
            loss = np.zeros([nregs, ncomp])




            ypred = np.zeros([c_test.shape[0], len(self.exp.compound_name)])




            for i_reg, mu in enumerate(self.regs):
                self.mm[i].load_attributes(theta=self.output+f"{iter_max}/mu_{mu}/nobs_{self.n_obs}_species_{species.lower()}_results.npz")
                for i_compound in range(ncomp):
                    self.mm[i].gen_Wv(comp = i_compound)                    
                    theta_norms= [npl.norm(self.mm[i].W[v,:,:].dot(self.mm[i].theta[i_compound][: ,v])) for v in range(len(self.mm[i].kept_P[i_compound]))]
                
                    lasso_path[i_reg, i_compound, self.mm[i].kept_kv[:,i_compound]] = np.array(theta_norms)
    
                for k, c, in enumerate(c_test.values):
                    ypred[k, :] = self.mm[i].flux_estimate(c[None, :])
                for l in range(ncomp):
                    loss[i_reg, l] = np.linalg.norm(ypred[:, self.exp.y_id(self.mm[i].compound_name[l])]-f_test[species][self.mm[i].compound_name[l]].values) / np.linalg.norm(f_test[species][self.mm[i].compound_name[l]].values)


            n_b=ncomp//2

            
            fig, axs = plt.subplots(n_b,3,figsize=(2*unit_width,n_b*unit_height),gridspec_kw={'width_ratios':[0.45,0.45,0.1]})
            for i_compound in range(ncomp):
                i_axs = i_compound%n_b
                j_axs = i_compound//n_b
                handles=[]
                for i_p in range(nparts):
                    p = parts[i_p]
                    if len(p.split('&'))==1:
                        curr_c = f'C{i_p}'
                        curr_ls='-'
                        curr_lw=1
                        curr_alpha=1
                        handles.append(mlines.Line2D([], [], color=curr_c, lw=curr_lw,ls=curr_ls,alpha=curr_alpha, label=p))
                    else:
                        if len(p.split('&'))==2:
                            curr_ls='--'
                            curr_lw='0.3'
                            curr_alpha=0.6
                        else:
                            curr_ls='-.'
                            curr_alpha=0.4
                        if compound_output[i_compound] in p:
                            curr_c='black'
                        else:
                            curr_c='lightgray'
                    lp = lasso_path[:, i_compound, i_p].T
                    axs[i_axs,j_axs].plot(self.regs, lp, color=curr_c, linewidth=curr_lw, ls = curr_ls, alpha=curr_alpha)
                axs[i_axs,j_axs].set_xlabel("$\mu$")
                axs[i_axs,j_axs].set_ylabel(r"$\sum_{v \in \mathcal{P}} ||\theta_v||_2$")
                axs[i_axs,j_axs].set_xlim(-0.025, np.max(self.regs)+0.025)
                ax2 = axs[i_axs,j_axs].twinx()
                ax2.set_ylabel("Test Loss",color='darkblue')
                ax2.set_yscale("log")
                ax2.set_ylim(5e-3, 1)
                loss_plot, = ax2.plot(self.regs, loss[ :, i_compound], 'darkblue', marker='*', linestyle='--', linewidth=1.5, markersize=6, label='loss')
                axs[i_axs,j_axs].set_title(compound_output[i_compound])
                # Create a legend for the first line.
                handles.append(mlines.Line2D([], [], color='black', lw=0.3,ls='--',alpha=0.6, label='Order2_inter'))
                handles.append(mlines.Line2D([], [], color='lightgray', lw=0.3,ls='--',alpha=0.6, label='Order2_other'))
                if self.max_order>2:
                    handles.append(mlines.Line2D([], [], color='black', lw=0.1,ls=':',alpha=0.4, label='Order3_inter'))
                    handles.append(mlines.Line2D([], [], color='lightgray', lw=0.1,ls=':',alpha=0.4, label='Order3_other'))



            for j_axs in range(n_b):
                axs[j_axs,-1].axis('off')
            first_legend = axs[0,-1].legend(handles=handles,loc='upper right')#, bbox_to_anchor=(1.05, 0.9))
                    # Add the legend manually to the Axes.
            axs[0,-1].add_artist(first_legend)

                    # Create another legend for the second line.
            axs[0,-1].legend(handles=[loss_plot],loc='lower right')#, bbox_to_anchor=(1.05, 0.05))
                #adapt_save_fig_top(fig, f"lasso_path_{s}.pdf")
            fig.tight_layout()
            fig.savefig(self.output+species+'_all_lassoPath.pdf')




if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Command description.")
    parser.add_argument(
        "-c",
        "--compute",
        action='store_true',
        help="compute metamodel for a grid of regulation parameters",
    )   
    parser.add_argument(
        "-p",
        "--plot",
        action='store_true',
        help="Plot lasso path for a grid of regulation parameters",
    )
    parser.add_argument(
        "-nobs",
        "--n_obs",
        help="Number of observations",
        type=int,
        default= 500 #400
    )    
    parser.add_argument(
        "-im",
        "--iter_max",
        help="maximal number of iterations during the learning",
        type=int,
        default=150 #20#150   ## This number has to be the same in the ode_metamodel file
    )
    parser.add_argument(
        "-mo",
        "--max_order",
        help="maximal interaction order in the Hoeffding decomposition",
        type=int,
        default=2
    )    
    parser.add_argument(
        "-g",
        "--grid",
        help="hyper parameter grid",
        default= '[.05,.075, .2, .3, .5, 1.]',#'[0.0, .001,.01, .05, .075,  .1, .2, .3, .4, .5, .6, .75, 1.]', This is the general case, but as we have already computed these values we only want the useful ones
        type=str,
    )    
    parser.add_argument(
        "-o",
        "--output_folder",
        help="Output folder",
        default='data/samples/hyperparameter_selection_identity/',
        type=str,
    )
    parser.add_argument(
        "-ode",
        "--config_ode",
        help="ode parameters: state variable, parameter of the general system (YAML)",
        default='data/EDO_parameters.yml',
        type=str,
    )
    parser.add_argument(
        "-exp",
        "--config_exp",
        help="dfba parameters: initial conditions and exchange reactions (YAML)",
        default="data/dFBA_styphi_fprau_oxygen_parameter.yml",
        type=str,
    )
    parser.add_argument(
        "-tw",
        "--type_weight",
        help="type of weight matrix in the learning regularization term. Can be either identity or KV_sqrt",
        default="identity",
        type=str,
    )    
    

    path_to_constraints = "data/samples/databases/database_subsample_2000_bounds_constraints.csv"
    path_to_fluxes_styphi = "data/samples/databases/database_subsample_2000_bounds_fluxes_Styphi.csv"
    path_to_fluxes_fprau = "data/samples/databases/database_subsample_2000_bounds_fluxes_Fprau.csv"

    path_to_fluxes = [path_to_fluxes_styphi, path_to_fluxes_fprau] # We are going to put it like for the moment
 
    args = parser.parse_args()
    grid=eval(args.grid)

    HPS = HyperParameterSelection(  path_to_constraints= path_to_constraints, 
                                    path_to_fluxes = path_to_fluxes, 
                                    regs=grid, 
                                    output = args.output_folder, 
                                    iter_max=args.iter_max, 
                                    n_obs=args.n_obs,
                                    config_ODE=args.config_ode,
                                    config_exp=args.config_exp,
                                    max_order=args.max_order,
                                   type_weight=args.type_weight
                                    )

    if args.compute:
        HPS.ComputeMetamodelHyperparameterGrid()

    if args.plot:
        HPS.PlotLassoPath()
